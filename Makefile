deploy: docker-push config
	@gcloud app deploy

config:
	@gcloud config set account gomezortiz.dev@gmail.com && \
	gcloud config set project calendars-289708

docker-push: docker-build
	@docker push eu.gcr.io/calendars-289708/calendars:latest

docker-build:
	@docker build -t eu.gcr.io/calendars-289708/calendars:latest .

.PHONY: deploy config docker-push docker-build