import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export const HOME_PATH = "/"
export const ONE_PATH = "/one"
export const TWO_PATH = "/two"
export const THREE_PATH = "/three"

const routes = [
    {
        path: HOME_PATH,
        name: "Home",
        component: () => import('@/views/Three')
    },
    {
        path: ONE_PATH,
        name: "Calendar 1",
        component: () => import('@/views/One')
    },
    {
        path: TWO_PATH,
        name: "Calendar 2",
        component: () => import('@/views/Two')
    },
    {
        path: THREE_PATH,
        name: "Calendar 3",
        component: () => import('@/views/Three')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })

export default router